#!/usr/bin/python

# import xml.etree.ElementTree as ET
from xml.dom import minidom
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring

import time
import os
import sys
import re
import netaddr
import fileinput
import hashlib
import shutil
import json   
from pprint import pprint
# Use 'pip install junos-eznc'
import getpass
from jnpr.junos import Device
from jnpr.junos.utils.config import Config

import config


def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def modify_file(file_name, pattern, value=""):
    fh = fileinput.input(file_name, inplace=True)
    for line in fh:
        replacement = line + value
        line = re.sub(pattern, replacement, line)
        sys.stdout.write(line)
    fh.close()


def delete_line(pattern):
    readFile = open(tempFeed, 'r')
    lines = readFile.readlines()
    readFile.close()

    writeFile = open(tempFeed, 'w')

    for line in lines:
        if line != pattern+"\n":
            #print "Not found--->" + line
            writeFile.write(line) 

    writeFile.close()
     

def update_manifest(feedname):
    ts = int(time.time())
    print("Modifying total number of objects for  -> " + feedname)
    with open(tempFeed, 'r') as feed:
        count = sum(1 for line in feed) - 5
        print("Revised address count of       -> " + str(count) + "\n")
    feed.close()  

    tree = ET.parse(tempManifest)
    root = tree.getroot()

    for feed in root.iter('feed'):
        name = feed.get('name')
        # print(name)
        if name == str(feedname):
            feed.set('data_ts', str(ts))
            feed.set('objects', str(count))

    tree.write(tempManifest) 


def create_manifest_entry(feedname): 
    ts = str(int(time.time()))
    print("Inserting new feed into manifest file located at " + config.feed_location)

    tree = ET.parse(tempManifest)
    root = tree.getroot()

    category = root.find('category')
    feed = ET.SubElement(category, 'feed', dict(data_ts=ts, name=feedname, objects="0", options="", types="ip_addr ip_range", version=feedname))
    data = ET.SubElement(feed, 'data')
    url = ET.SubElement(data, 'url')
    url.text = '/'

    text = (prettify(root))
    cleantext = "".join([s for s in text.strip().splitlines(True) if s.strip()])

    with open(tempManifest, 'w') as file:
        file.write(cleantext)


def copy_feed_to_tempFeed():
    shutil.copyfile(feed, tempFeed)
    readFile = open(tempFeed)
    lines = readFile.readlines()
    readFile.close()
    writeFile = open(tempFeed, 'w')
    writeFile.writelines([item for item in lines[:-1]])
    writeFile.close()


def copy_tempFeed_to_feed():
    shutil.copyfile(tempFeed, feed)


def copy_tempManifest_to_Manifest():
    shutil.copyfile(tempManifest, manifest)


def copy_Manifest_to_tempManifest():
    shutil.copyfile(manifest, tempManifest)


def create_newFeed(name):
    shutil.copyfile('Feed', name)


def calculate_md5():
    with open(tempFeed, 'rb') as file:
        data = file.read()
        md5_returned = hashlib.md5(data).hexdigest()
        file.close()
  
    writeFile = open(tempFeed, 'a')
    writeFile.write(md5_returned)
    writeFile.close()


def run_setup():
    script_path = os.path.dirname(sys.argv[0])
    shutil.copyfile(script_path + '/manifest.xml', config.feed_location + 'manifest.xml')
    shutil.copyfile(script_path + '/schema.xml', config.feed_location + 'schema.xml')


def main(input_operation, input_feed, input_address):
    global feed
    global tempFeed
    global manifest
    global tempManifest
    feed = config.feed_location + str(input_feed)
    tempFeed = config.temp_location + str(input_feed)
    manifest = config.feed_location + 'manifest.xml'
    tempManifest = config.temp_location + 'manifest.xml'

    if input_operation == 'add':
        copy_feed_to_tempFeed()
        copy_Manifest_to_tempManifest()
        ip = netaddr.IPNetwork(input_address)
        feedname = input_feed
        address = ip.ip
        size = ip.size 
        adj_size = size -1 
        value = ip.value
        print("\nAdding address of              -> " + str(input_address) +" (including " + str(adj_size) + " subequent hosts)")

        if adj_size == 0: 
            newentry = '{"1":' + str(value) +'}'
        else: 
            newentry = '{"2":[' + str(value) + ',' + str(adj_size) +']}'

        # print(newentry)
        modify_file(tempFeed, '#add', newentry)
        calculate_md5()
        update_manifest(feedname)
        copy_tempFeed_to_feed()
        copy_tempManifest_to_Manifest()

    if input_operation == 'del':
        copy_feed_to_tempFeed()
        copy_Manifest_to_tempManifest()
        ip = netaddr.IPNetwork(input_address)
        feedname = input_feed
        address = ip.ip
        size = ip.size
        adj_size = size -1 
        value = ip.value
        print("\nRemoving address of            -> " + str(input_address) +" (including " + str(adj_size) + " subequent hosts)")

        if adj_size == 0:
            oldline = '{"1":' + str(value) +'}'
        else:
            oldline = '{"2":[' + str(value) + ',' + str(adj_size) +']}'
            delete_line(oldline)

        calculate_md5()
        update_manifest(feedname)
        copy_tempFeed_to_feed()
        copy_tempManifest_to_Manifest()

    if input_operation == 'list':

        pattern_network = '{"(\d+)":\[\d+,\d+\]}'
        pattern_host = '{"(\d+)":\d+}'
        pattern_ip_network = '{"\d+":\[(\d+),\d+]}'
        pattern_ip_host = '{"\d+":(\d+)}'
        pattern_range = '\d+":\[\d+,(\d+)]}'

        with open(feed, 'r') as file:
            lines = file.readlines()
     
        for line in lines:
            host = re.search(pattern_host, line)
            network = re.search(pattern_network, line)
        if host: 
            ip = str(netaddr.IPAddress(re.findall(pattern_ip_host, line)[0]))
            print("Host entry:    " + ip)

        elif network: 
            # ip = re.findall(pattern_ip_network, line)[0]
            ip = str(netaddr.IPAddress(re.findall(pattern_ip_network, line)[0]))
            range = re.findall(pattern_range, line)[0]
            print("Network Entry: " + ip + " (+" + range + " hosts)")

    if input_operation == 'new':
        name = str(input_feed)
        copy_Manifest_to_tempManifest()
        print(name)
        create_newFeed(feed)
        create_manifest_entry(name)
        copy_tempManifest_to_Manifest()
        print("Completed, add the following line to your SRX to accept feed:\n set security dynamic-address address-name "+name+ " profile category IPFilter feed "+name)
        username = input("Please enter your SRX Username:")
        password = getpass.getpass()
        srx_list = 'srx-list'
        srxs = open(srx_list, 'r')
        for srx in srxs:
            print("Logging into SRX "+srx)
            login = str(srx)
            dev = Device(host=login, user=username, password=password)
            dev.open()
            dev.timeout = 300
            cu = Config(dev)
            set_cmd = 'set security dynamic-address address-name '+name+' profile category IPFilter feed '+name
            cu.load(set_cmd, format='set')
            print("Applying changes, please wait....")
            cu.commit()
            dev.close()

    if input_operation == 'setup':
        print("Kick off initial setup process, copy files to target directories etc")
        run_setup()


if __name__ == "__main__":
    if len(sys.argv) == 1:
        menu = True
        while menu:
            print("""
            1.Create a new Feed (address-set)
            2.List the addresses in a feed
            3.Add an address to a feed
            4.Removed an address from a feed
            5.Setup (Copy the manifest and schema files to Web Root)
            q.Exit
            """)
            ans = input("What would you like to do? ")
            if ans == "1":
                input_operation = 'new'
                print("\n You chose to Create a new Feed")
                input_address = 'null'
                menu = False
            elif ans == "2":
                input_operation = 'list'
                print("\n You chose to List addresses in a feed")
                input_address = 'null'
                menu = False
            elif ans == "3":
                input_operation = 'add'
                print("\n You chose to add an address to a feed")
                input_address = input("Enter the address you wish to add: ")
                menu = False
            elif ans == "5":
                input_operation = 'setup'
                print("\nYou chose to run setup, this will override your existing schema & manifest files...")
                input_address = 'null'
                run_setup() if (input("\nDo you wish to continue(y/n)? ") == 'y') else menu
            elif ans == "q":
                print("\n Goodbye") 
                sys.exit(0)
            else:
                print("\n Not Valid Choice Try again")
        input_feed = input("Enter the name of the Feed: ")
    else:
        input_operation = sys.argv[1]
        input_feed = sys.argv[2] if sys.argv[2] else 'null'
        input_address = sys.argv[3] if sys.argv[3] else 'x.x.x.x/x'

    main(input_operation, input_feed, input_address)
