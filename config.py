# Folder location of the Web Server Root
feed_location = 'C:/temp/www/'
# Folder location to write temporary files
temp_location = 'C:/temp/tmp/'

# Default feed name (AKA: address-set that will hold the address book entries)
feedname = 'TESTFEED'
