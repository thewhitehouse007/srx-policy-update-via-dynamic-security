import json
import os
import sys
import urllib.request
import uuid
import datetime
import dynamicpolicy
import config

# helper to call the webservice and parse the response
def webApiGet(methodName, instanceName, clientRequestId):
    ws = "https://endpoints.office.com"
    requestPath = ws + '/' + methodName + '/' + instanceName + '?clientRequestId=' + clientRequestId
    request = urllib.request.Request(requestPath)
    with urllib.request.urlopen(request) as response:
        return json.loads(response.read().decode())


# method to call dynamicpolicy.py and update the manifest with new IPs
def build_endpoint_manifest(feedname, datapath):
    print("Modifying total number of objects for  -> " + feedname + " from " + datapath)
    with open(datapath, 'r') as endpoints:
        # if not os.path.isfile(config.feed_location + config.feedname):
        dynamicpolicy.main('new', feedname, '')
        count = 0
        for endpoint in endpoints:
            dynamicpolicy.main('add', feedname, endpoint)
            count += 1
        print("Revised address count of       -> " + str(count) + "\n")
        dynamicpolicy.main('list', feedname, '')


# File where client ID and latest version number will be stored
versionpath = os.path.dirname(sys.argv[0]) + '\endpoints_clientid_latestversion.txt'
# File for raw IP Address subnets are stored for processing
datapath = os.path.dirname(sys.argv[0]) + '\endpoints_data.txt'
# Log File for output when script is run as a Scheduled task
scriptlog = os.path.dirname(sys.argv[0]) + '\endpoints_import.log'
# Globalizing variable from config file
feedname = config.feedname

# fetch client ID and version if data exists; otherwise create new file
if os.path.exists(versionpath):
    with open(versionpath, 'r') as fin:
        clientRequestId = fin.readline().strip()
        latestVersion = fin.readline().strip()
else:
    clientRequestId = str(uuid.uuid4())
    latestVersion = '0000000000'
    with open(versionpath, 'w') as fout:
        fout.write(clientRequestId + '\n' + latestVersion)

# call version method to check the latest version, and pull new data if version number is different
version = webApiGet('version', 'Worldwide', clientRequestId)
if version['latest'] > latestVersion:
    with open(scriptlog, 'a') as sdout:
        sdout.write('{0} {1}\n'.format(datetime.datetime.now(), 'A new version of Office 365 worldwide commercial service instance endpoints was detected'))
    # write the new version number to the data file
    with open(versionpath, 'w') as fout:
        fout.write(clientRequestId + '\n' + version['latest'])
    # invoke endpoints method to get the new data
    endpointSets = webApiGet('endpoints', 'Worldwide', clientRequestId)
    # filter results for Allow and Optimize endpoints, and transform these into tuples with category
    flatIps = []
    for endpointSet in endpointSets:
        if endpointSet['category'] in ('Optimize', 'Allow'):
            ips = endpointSet['ips'] if 'ips' in endpointSet else []
            category = endpointSet['category']
            # IPv4 strings have dots while IPv6 strings have colons
            ip4s = [ip for ip in ips if '.' in ip]
            flatIps.extend([(category, ip) for ip in ip4s])
    with open(datapath, 'w') as fout:
        fout.write('\n'.join(sorted(set([ip for (category, ip) in flatIps]))))
    print('\n'.join(sorted(set([ip for (category, ip) in flatIps]))))
    build_endpoint_manifest(feedname, datapath)
else:
    with open(scriptlog, 'a') as sdout:
        sdout.write('{0} {1}\n'.format(datetime.datetime.now(), 'Office 365 worldwide commercial service instance endpoints are up-to-date'))
